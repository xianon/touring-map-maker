#!/usr/bin/env python
# -*- coding: utf-8 -*-
from distutils.util import strtobool
from urllib.parse import urlencode, urljoin, urlparse

import requests

from app.models import User, db
from flask import (Blueprint, abort, current_app, redirect, render_template,
                   request, session, url_for)
from flask_login import LoginManager, current_user, login_user, logout_user

login_manager = LoginManager()
login_manager.login_view = 'auth.login'

auth = Blueprint('auth', __name__)

def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and ref_url.netloc == test_url.netloc

@login_manager.user_loader
def load_user(user_id):
    try:
        user_id = int(user_id)
    except ValueError:
        return None

    access_token = session.get('access_token')
    if access_token is None:
        return None

    user = User.query.get(user_id)
    if user is None:
        return None

    domain = current_app.config['DOMAIN']
    verify_credentials_url = f'https://{domain}/api/v1/accounts/verify_credentials'
    headers = {
        'Authorization': f'Bearer {access_token}',
    }
    try:
        response = requests.get(verify_credentials_url, headers=headers)
        response.raise_for_status()
    except requests.exceptions.RequestException:
        return None

    account_json = response.json()
    if account_json is None:
        return None

    try:
        account_id = int(account_json.get('id'))
    except ValueError:
        account_id = None
    user_name = account_json.get('username')
    if user.account_id != account_id or user.name != user_name:
        return None

    has_changed = False

    display_name = account_json.get('display_name')
    if display_name == '':
        display_name = None
    if user.display_name != display_name:
        user.display_name = display_name
        has_changed = True

    if has_changed:
        try:
            db.session.commit()
        except:
            return None

    return user

@auth.route('/login')
def login():
    if current_user.is_authenticated:
        next = session.pop('next', None)
        if not is_safe_url(next):
            return abort(requests.codes['bad_request'])

        return redirect(next or url_for('map.top'))
    elif 'code' in request.args:
        authorization_code = request.args.get('code')
        domain = current_app.config['DOMAIN']
        token_url = f'https://{domain}/oauth/token'
        parameters = {
            'client_id': current_app.config['CLIENT_ID'],
            'client_secret': current_app.config['CLIENT_SECRET'],
            'grant_type': 'authorization_code',
            'code': authorization_code,
            'redirect_uri': url_for('auth.login', _external=True),
        }
        try:
            response = requests.post(token_url, data=parameters)
            response.raise_for_status()
        except requests.exceptions.RequestException:
            return abort(requests.codes['internal_server_error'])

        token_json = response.json()
        if token_json is None:
            return abort(requests.codes['internal_server_error'])

        access_token = token_json.get('access_token')
        if access_token is None:
            return abort(requests.codes['internal_server_error'])

        verify_credentials_url = f'https://{domain}/api/v1/accounts/verify_credentials'
        headers = {
            'Authorization': f'Bearer {access_token}',
        }
        try:
            response = requests.get(verify_credentials_url, headers=headers)
            response.raise_for_status()
        except requests.exceptions.RequestException:
            return abort(requests.codes['internal_server_error'])

        account_json = response.json()
        if account_json is None:
            return abort(requests.codes['internal_server_error'])

        try:
            account_id = int(account_json.get('id'))
        except ValueError:
            account_id = None
        user_name = account_json.get('username')
        if account_id is None or user_name is None:
            return abort(requests.codes['internal_server_error'])

        user = User.query.filter_by(name=user_name).first()
        if user is None:
            user = User()
            db.session.add(user)
            user.account_id = account_id
            user.name = user_name
        elif user.account_id != account_id:
            return abort(requests.codes['internal_server_error'])

        user.display_name = account_json.get('display_name')
        if user.display_name == '':
            user.display_name = None
        try:
            db.session.commit()
        except:
            return abort(requests.codes['internal_server_error'])

        session['access_token'] = access_token
        login_user(user)

        next = session.pop('next', None)
        if not is_safe_url(next):
            return abort(requests.codes['bad_request'])

        return redirect(next or url_for('map.top'))
    elif 'error' in request.args:
        application_title = current_app.config['APPLICATION_TITLE']
        error_message = f"{request.args['error']}: {request.args.get('error_description', '')}"
        return render_template('auth/error.html', application_title=application_title, error_message=error_message)

    try:
        oauth = strtobool(request.args.get('oauth', 'False'))
    except ValueError:
        oauth = False
    if oauth:
        domain = current_app.config['DOMAIN']
        authorize_url = f'https://{domain}/oauth/authorize'
        parameters = {
            'client_id': current_app.config['CLIENT_ID'],
            'response_type': 'code',
            'redirect_uri': url_for('auth.login', _external=True),
            'scope': 'read:accounts',
        }
        return redirect(f'{authorize_url}?{urlencode(parameters)}')
    else:
        application_title = current_app.config['APPLICATION_TITLE']
        instance_name = current_app.config['INSTANCE_NAME']
        domain = current_app.config['DOMAIN']
        instance_url = f'https://{domain}/'
        return render_template('auth/login.html', application_title=application_title, instance_name=instance_name, instance_url=instance_url)

@auth.route('/logout')
def logout():
    if not current_user.is_authenticated:
        return redirect(url_for('map.top'))

    access_token = session.pop('access_token', None)
    if access_token is not None:
        domain = current_app.config['DOMAIN']
        token_url = f'https://{domain}/oauth/revoke'
        headers = {
            'Authorization': f'Bearer {access_token}',
        }
        parameters = {
            'client_id': current_app.config['CLIENT_ID'],
            'client_secret': current_app.config['CLIENT_SECRET'],
            'token': access_token,
        }
        try:
            response = requests.post(token_url, headers=headers, data=parameters)
            response.raise_for_status()
        except requests.exceptions.RequestException:
            return abort(requests.codes['internal_server_error'])

    logout_user()

    return redirect(url_for('map.top'))
