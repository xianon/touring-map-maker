#!/usr/bin/env python
# -*- coding: utf-8 -*-
from urllib.parse import urlencode

import requests

from app.models import Area, Spot, User, db
from flask import Blueprint, abort, current_app, render_template, session
from flask_login import current_user

map = Blueprint('map', __name__)

@map.route('/')
def top():
    domain = current_app.config['DOMAIN']

    if current_user.is_authenticated:
        access_token = session.get('access_token')
        if access_token is None:
            return abort(requests.codes['internal_server_error'])

        parameters = {
            'limit': 80,
        }
        followers_url = f'https://{domain}/api/v1/accounts/{current_user.account_id}/followers?{urlencode(parameters)}'
        headers = {
            'Authorization': f'Bearer {access_token}',
        }
        follower_ids = []
        while True:
            try:
                response = requests.get(followers_url, headers=headers)
                response.raise_for_status()
            except requests.exceptions.RequestException:
                return abort(requests.codes['internal_server_error'])

            account_jsons = response.json()
            if not isinstance(account_jsons, list):
                return abort(requests.codes['internal_server_error'])

            for account_json in account_jsons:
                try:
                    account_id = int(account_json.get('id'))
                except ValueError:
                    continue
                account_acct = account_json.get('acct')
                if account_acct is None or '@' in account_acct:
                    continue
                follower_ids.append(account_id)

            next_link = response.links.get('next')
            if next_link is None:
                break
            followers_url = next_link.get('url')
            if followers_url is None:
                break

        user_subquery = User.query\
            .filter(db.or_(User.id == current_user.id, User.account_id.in_(follower_ids)))\
            .filter(User.area_id == Area.id)\
            .exists()
        user_areas = Area.query\
            .filter(user_subquery)\
            .options(db.joinedload(Area.users))\
            .order_by(Area.id)\
            .all()

        areas = Area.query\
            .options(db.noload(Area.users))\
            .options(db.noload(Area.prefecture))\
            .options(db.noload(Area.city))\
            .options(db.noload(Area.cities))\
            .options(db.noload(Area.wards))\
            .order_by(Area.id)\
            .all()
        prefectures = []
        cities = []
        wards = []
        for area in areas:
            if area.prefecture_id is None and area.city_id is None:
                prefectures.append(area)
            elif area.prefecture_id is not None and area.city_id is None:
                cities.append(area)
            elif area.prefecture_id is not None and area.city_id is not None:
                wards.append(area)
        for prefecture in prefectures:
            for city in cities:
                if city.prefecture_id == prefecture.id:
                    city.prefecture = prefecture
                    prefecture.cities.append(city)
            for ward in wards:
                if ward.prefecture_id == prefecture.id:
                    ward.prefecture = prefecture
                    prefecture.wards.append(ward)
        for city in cities:
            for ward in wards:
                if ward.city_id == city.id:
                    ward.city = city
                    city.wards.append(ward)
    else:
        user_areas = []

    spots = Spot.query.all()

    application_title = current_app.config['APPLICATION_TITLE']
    instance_url = f'https://{domain}/'
    mapbox_user_name = current_app.config['MAPBOX_USER_NAME']
    mapbox_style_id = current_app.config['MAPBOX_STYLE_ID']
    mapbox_access_token = current_app.config['MAPBOX_ACCESS_TOKEN']
    return render_template('map/top.html', application_title=application_title, instance_url=instance_url, mapbox_user_name=mapbox_user_name, mapbox_style_id=mapbox_style_id, mapbox_access_token=mapbox_access_token, user_areas=user_areas, spots=spots)
