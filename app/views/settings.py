#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests

from app.models import Area, db
from flask import (Blueprint, abort, current_app, flash, redirect,
                   render_template, request, url_for)
from flask_login import current_user, login_required

settings = Blueprint('settings', __name__)

@settings.route('/settings/')
@login_required
def top():
    return redirect(url_for('settings.habitat'))

@settings.route('/settings/habitat', methods=['GET', 'POST'])
@login_required
def habitat():
    if request.method == 'POST':
        area_id = request.form['wardid']\
            or request.form['cityid']\
            or request.form['prefectureid']
        if area_id is not None:
            current_user.area_id = area_id
            try:
                db.session.commit()
            except:
                return abort(requests.codes['internal_server_error'])
        flash('設定を保存しました。', 'success')

    application_title = current_app.config['APPLICATION_TITLE']

    prefectures = Area.query\
        .options(db.joinedload(Area.cities).joinedload(Area.wards))\
        .filter(Area.prefecture_id == None, Area.city_id == None)\
        .order_by(Area.id)\
        .all()

    cities = []
    for prefecture in prefectures:
        for city in sorted(prefecture.cities, key=lambda city: city.id):
            cities.append(city)

    wards = []
    for city in cities:
        for ward in sorted(city.wards, key=lambda ward: ward.id):
            wards.append(ward)

    area = current_user.area
    if area is None:
        prefecture_id = None
        city_id = None
        ward_id = None
    elif area.prefecture_id is None:
        prefecture_id = area.id
        city_id = None
        ward_id = None
    elif area.city_id is None:
        prefecture_id = area.prefecture_id
        city_id = area.id
        ward_id = None
    else:
        prefecture_id = area.prefecture_id
        city_id = area.city_id
        ward_id = area.id

    return render_template('settings/habitat.html', application_title=application_title, prefectures=prefectures, cities=cities, wards=wards, prefecture_id=prefecture_id, city_id=city_id, ward_id=ward_id)
