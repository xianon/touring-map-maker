#!/usr/bin/env python
# -*- coding: utf-8 -*-
from app.models import db
from app.views.auth import auth, login_manager
from app.views.map import map
from app.views.settings import settings
from flask import Flask
from flask_migrate import Migrate
from flask_sslify import SSLify

app = Flask(__name__)
app.config.from_object('config')
sslify = SSLify(app)

db.init_app(app)
migrate = Migrate(app, db)

login_manager.init_app(app)

app.register_blueprint(auth)
app.register_blueprint(map)
app.register_blueprint(settings)
