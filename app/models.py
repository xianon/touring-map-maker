#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_login import UserMixin
from flask_sqlalchemy import SQLAlchemy
from geoalchemy2.types import Geometry

db = SQLAlchemy()

class GeometryJson(Geometry):
    as_binary = 'ST_AsGeoJSON'

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    account_id = db.Column(db.BigInteger, index=True, nullable=False, unique=True)
    name = db.Column(db.String, index=True, nullable=False, unique=True)
    display_name = db.Column(db.String)
    area_id = db.Column(db.Integer, db.ForeignKey('area.id'))

    area = db.relationship('Area', back_populates='users')

class Area(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=False)
    prefecture_id = db.Column(db.Integer, db.ForeignKey('area.id'))
    city_id = db.Column(db.Integer, db.ForeignKey('area.id'))
    name = db.Column(db.String, nullable=False)

    prefecture = db.relationship('Area', foreign_keys=[prefecture_id], remote_side=[id])
    city = db.relationship('Area', foreign_keys=[city_id], remote_side=[id])
    cities = db.relationship('Area', primaryjoin=db.and_(prefecture_id == None, city_id == None, db.remote(prefecture_id) == id, db.remote(city_id) == None), order_by=id)
    wards = db.relationship('Area', primaryjoin=db.or_(db.and_(prefecture_id == None, city_id == None, db.remote(prefecture_id) == id, db.remote(city_id) != None), db.and_(prefecture_id != None, city_id == None, db.remote(prefecture_id) != None, db.remote(city_id) == id)), order_by=id)
    users = db.relationship('User', back_populates='area', order_by=User.id)

class Spot(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    description = db.Column(db.String)
    geometry = db.Column(GeometryJson(geometry_type='POINT', srid=4326), nullable=False)
