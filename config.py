#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os

from app import app

SECRET_KEY = os.environ['SECRET_KEY']

USE_SESSION_FOR_NEXT = True

SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
SQLALCHEMY_TRACK_MODIFICATIONS = False
SQLALCHEMY_ECHO = app.config['DEBUG']

APPLICATION_TITLE = os.environ['APPLICATION_TITLE']

INSTANCE_NAME = os.environ['INSTANCE_NAME']
DOMAIN = os.environ['DOMAIN']

CLIENT_ID = os.environ['CLIENT_ID']
CLIENT_SECRET = os.environ['CLIENT_SECRET']

MAPBOX_USER_NAME = os.environ['MAPBOX_USER_NAME']
MAPBOX_STYLE_ID = os.environ['MAPBOX_STYLE_ID']
MAPBOX_ACCESS_TOKEN = os.environ['MAPBOX_ACCESS_TOKEN']
